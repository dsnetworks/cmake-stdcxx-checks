# CMake StdCxxChecks module
This simple CMake module provides a macro and some C++ source files to check for various modern C++ features:

* `std::quoted`
* `std::put_time`
* `std::integer_sequence`

## Example
```
# cmake
include(StdCxxChecks)
check_std_cxx_features(std::integer_sequence std::quoted std::put_time)

if(STDCXX_HAVE_std_quoted)
	# ...
endif(STDCXX_HAVE_std_quoted)
```
