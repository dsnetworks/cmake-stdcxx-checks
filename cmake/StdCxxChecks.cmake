if(NOT DEFINED STD_CXX_CHECK_SOURCE_DIR)
    set(STD_CXX_CHECK_SOURCE_DIR ${CMAKE_CURRENT_LIST_DIR}/stdcxx-checks)
endif(NOT DEFINED STD_CXX_CHECK_SOURCE_DIR)

if(NOT DEFINED STD_CXX_CHECK_BINARY_DIR)
    set(STD_CXX_CHECK_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}/stdcxx-checks)
endif(NOT DEFINED STD_CXX_CHECK_BINARY_DIR)

function(check_std_cxx_features)
    foreach(FEATURE ${ARGN})
        string(REPLACE "::" "_" STD_CXX_FEATURE ${FEATURE})
        set(STD_CXX_FEATURE_VARIABLE "STDCXX_HAVE_${STD_CXX_FEATURE}")
        if(NOT DEFINED ${STD_CXX_FEATURE_VARIABLE})
            set(STD_CXX_TEST_FILE "${STD_CXX_CHECK_SOURCE_DIR}/${STD_CXX_FEATURE}.cpp")

            if(NOT EXISTS ${STD_CXX_TEST_FILE})
                message(FATAL_ERROR "StdCxxChecks: Expected feature test ${STD_CXX_TEST_FILE} doesn't exist!")
            endif(NOT EXISTS ${STD_CXX_TEST_FILE})

            try_compile(STD_CXX_TEST_RESULT ${STD_CXX_CHECK_BINARY_DIR}/${STD_CXX_FEATURE} ${STD_CXX_TEST_FILE})

            set(${STD_CXX_FEATURE_VARIABLE} ${STD_CXX_TEST_RESULT} CACHE BOOL "C++ has support for ${FEATURE}" FORCE)
            set(${STD_CXX_FEATURE_VARIABLE} ${STD_CXX_TEST_RESULT} PARENT_SCOPE)
            mark_as_advanced(${STD_CXX_FEATURE_VARIABLE})

            message(STATUS "StdCxxChecks: ${FEATURE} -> ${STD_CXX_FEATURE_VARIABLE} = ${STD_CXX_TEST_RESULT}")
        endif(NOT DEFINED ${STD_CXX_FEATURE_VARIABLE})
    endforeach(FEATURE)
endfunction(check_std_cxx_features)
