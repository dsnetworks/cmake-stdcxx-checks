#include <iostream>
#include <utility>

template <typename F, typename T> void for_each_element_impl(std::index_sequence<>, const F&, const T&)
{ /* nothing */
}

template <typename F, typename T, std::size_t I, std::size_t... Is>
void for_each_element_impl(std::index_sequence<I, Is...>, const F& functor, const T& tuple)
{
    functor(std::get<I>(tuple));
    for_each_element_impl(std::index_sequence<Is...>(), functor, tuple);
}

template <typename F, typename T> void for_each_element(const F& functor, const T& tuple)
{
    constexpr const auto N = std::tuple_size<T>::value;
    for_each_element_impl(std::make_index_sequence<N>(), functor, tuple);
}

int main()
{
    auto functor = [](auto value) { std::cout << __PRETTY_FUNCTION__ << " value: " << value << std::endl; };
    auto tuple   = std::make_tuple(23, 42, 13.37f, "hello, world");

    for_each_element(functor, tuple);

    return 0;
}
