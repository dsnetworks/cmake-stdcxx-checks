#include <iomanip>
#include <iostream>
#include <string>

int main()
{
    std::string test("test string");
    std::cout << "testing std::quoted: " << std::quoted(test) << std::endl;
    return 0;
}
