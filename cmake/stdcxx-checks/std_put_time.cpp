#include <iomanip>
#include <iostream>

int main()
{
    std::cout << std::put_time(nullptr, "%H:%M:%S") << std::endl;
    return 0;
}
